package com.posa.hotel.service;


import com.posa.hotel.dao.User;
import com.posa.hotel.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private LoggedUserHolder loggedUserHolder;

    public boolean loginUser(String username, String password) {
        User user = userDao.getUserByUsername(username);
        if(user == null) {
            return false;
        }
        if(user.getPassword().equals(password)) {
            loggedUserHolder.setUserId(user.getId());
            return true;
        } else {
            return false;
        }
    }

    public boolean isUserLoggedIn() {
        return loggedUserHolder.getUserId() > 0;
    }
}
