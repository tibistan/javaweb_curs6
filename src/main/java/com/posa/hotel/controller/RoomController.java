package com.posa.hotel.controller;

import com.posa.hotel.service.LoggedUserHolder;
import com.posa.hotel.service.LoginService;
import com.posa.hotel.service.RoomService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.datetime.joda.DateTimeParser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.sql.Date;

@Controller
public class RoomController {


    @Autowired
    private RoomService roomService;

    @Autowired
    private LoginService loginService;

    @Autowired
    private LoggedUserHolder loggedUserHolder;

    @RequestMapping(value = "/bookRoom")
    public ModelAndView bookRoom(@RequestParam("dataStart") String dateStartString,
                                 @RequestParam("dataEnd") String dateEndString,
                                 @RequestParam("roomId") Integer roomId) {

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate dateStart = LocalDate.parse(dateStartString, dateTimeFormatter);
        LocalDate dateEnd = LocalDate.parse(dateEndString, dateTimeFormatter);

        roomService.bookRoom(dateStart, dateEnd, roomId, loggedUserHolder.getUserId());
        ModelAndView modelAndView = new ModelAndView("book_success");
        return modelAndView;
    }

    @RequestMapping(value = "/book")
    public ModelAndView selectRoom(@RequestParam("roomId") Integer id) {
        ModelAndView modelAndView = new ModelAndView("room");
        modelAndView.addObject("roomId",id);
        return modelAndView;
    }

    @RequestMapping(value = "/rooms")
    public ModelAndView viewAllRooms() {
        if(!loginService.isUserLoggedIn()) {
            return new ModelAndView("redirect:/login.html");
        }

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("rooms", roomService.getAllRooms());

        return modelAndView;
    }
}
